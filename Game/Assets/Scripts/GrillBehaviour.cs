﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrillBehaviour : MonoBehaviour
{
    private float grillStatus;
    private float timer = 0.0f;
    private bool colliding = false;
    private Collision customCollider;
    private bool isCooked;

    //RGB: 183, 90, 28
    //RAW ONES: RGB: 224, 168, 130
    public Color colorEnd;
    private Renderer rend;
    float startTime;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (colliding && isCooked == false)
        {
            timer += Time.deltaTime;
            float t = Time.deltaTime / 5.0f; //this is provisoric.
            customCollider.collider.GetComponent<Material>();
            rend.material.color = Color.Lerp(rend.material.color, colorEnd, t);

            if (timer >= 10.0f)
            {
                isCooked = true;
                Debug.Log("Cooking done.");
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.name == "grill")
        {
            colliding = true;
            customCollider = collision;
        }

    }


    void OnCollisionExit(Collision collision)
    {
        if (collision.collider.name == "grill")
        {
            colliding = false;
            customCollider = null;
        }
    }

}
