﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotDogBuilding : MonoBehaviour
{
    private GameObject sausage;
    private bool hotDogDone;
    private Customer customer;


    // Start is called before the first frame update
    void Start()
    {
        customer = GameObject.Find("Customer").GetComponent<Customer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(hotDogDone == true){
            //we are still colliding, which means, the hotdog is done.
            customer.GetQuest().setIsCompleted();


        }else{

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.name.Contains("sausage")){
            sausage = collision.collider.gameObject;
            hotDogDone = true;
            sausage.transform.parent = this.transform;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.collider.name.Contains("sausage")){
            hotDogDone = false;
        }
    }
}
