﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactOnDecision : MonoBehaviour
{
    private GameObject customer;
    private Customer customerClass;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void QuestAccepted(){


        customer = GameObject.Find("Customer");
        customerClass = customer.GetComponent<Customer>();
        customer.SendMessage("DisplayQuest", false);
        GameObject.Find("CharacterCamera").SendMessage("resetIsVisibleVar");

        //set the accepted quest to the current quest.
        GameObject.Find("Character").SendMessage("currentQuest", customerClass.GetQuest());




    }

    public void QuestDeclined(){

    }

}
