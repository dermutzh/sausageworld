﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour
{

    private string ANname;
    private Quest quest;

    public List<Transform> waypoints;
    int current = 0;
    float wpRadius = 0.5f;
    float speed = 5.0f;
    private GameObject questUI;
    private GameObject questDescription;
    private bool isVisible;
    private bool customerGotHotDog;
    private GameObject infoPanelLevel;
    private GameObject infoPanelMoney;
    private GameObject infoPanelXP;
    private GameObject activeQuestPanel;
    //endpoint, where customer should wait:
    // XYZ: 2, 2, -8

    // Start is called before the first frame update
    void Start()
    {
        quest = new Quest();
        questUI = GameObject.Find("Quest_Background");
        questDescription = GameObject.Find("Quest_Description");
        questUI.SetActive(false);
        customerGotHotDog = false;
        infoPanelLevel = GameObject.Find("LevelText");
        infoPanelXP = GameObject.Find("XPText");
        infoPanelMoney = GameObject.Find("MoneyText");
        activeQuestPanel = GameObject.Find("ActiveQuest");
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, waypoints[current].position) <= wpRadius){

            current++;
            if(current >= waypoints.Count){
                //destroy customer here, and initalize a new one.
            }
        }


        if (current == 2 && customerGotHotDog == false)
        {
            //wait on position, till quest is completed. if its completed, move on.

        }
        else{
        
            //quest is complete
            if(quest.getIsCompleted()){
                infoPanelXP.GetComponent<Text>().text = quest.getXpPoints() + " / 150";
                infoPanelMoney.GetComponent<Text>().text = quest.getEarnedMoney() + " €";
            }
           
            transform.position = Vector3.MoveTowards(transform.position, waypoints[current].position, speed * Time.deltaTime);
        }

    }

    void DisplayQuest(bool visibility){
        if(visibility == true){
            questDescription.GetComponent<Text>().text = quest.getDescription();
            questUI.SetActive(true);
        }else{
            questUI.SetActive(false);
        }

    }

    public Quest GetQuest(){
        return quest;
    }


    public void setCustomerGotHotDog(){
        customerGotHotDog = true;
    }

}
