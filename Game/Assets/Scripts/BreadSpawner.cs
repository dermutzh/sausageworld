﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadSpawner : MonoBehaviour
{
    public GameObject bread;
    // Start is called before the first frame update
    void Start()
    {
            Instantiate(bread, new Vector3(6.25f, 3.5f, -5.5f), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
