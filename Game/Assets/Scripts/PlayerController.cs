﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

 
    private float speed = 10.0f;
    private Quest myquest;
    private int currentXp;
    private int level;
    private int money;
    private GameObject activeQuest;
    private GameObject activeQuestDescription;
    private GameObject activeQuestCount;
    private int countOfCompletedHotDogs;
    private 

    // Use this for initialization
    void Start()
    {
        activeQuest = GameObject.Find("ActiveQuest");
        activeQuestDescription = GameObject.Find("QuestRequiredHotDogs");
        activeQuestCount = GameObject.Find("QuestRequiredHotDogsCount");
        activeQuest.SetActive(false);
        countOfCompletedHotDogs = 0;
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    private void FixedUpdate()
    {
        float hAxis = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float vAxis = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        transform.Translate(hAxis, 0, vAxis);
     
    }


    void currentQuest(Quest currentQuest){
        //here we fill the Active Quest UI with some information we get from the Customer.
        GameObject.Find("CharacterCamera").SendMessage("DisableMovingAround", false);
        activeQuestDescription.GetComponent<Text>().text = currentQuest.getWhatCustomerWants();
        activeQuestCount.GetComponent<Text>().text = countOfCompletedHotDogs + " / " + currentQuest.getRequiredHotDogs();
        activeQuest.SetActive(true);

    }

    public void QuestDone(){
        activeQuest.SetActive(false);
    }
}
