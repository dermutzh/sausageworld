﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SausageSpawner : MonoBehaviour
{
    public GameObject sausage;

    //Colorcode #B75A1C
    // Start is called before the first frame update
    void Start()
    {
     for(int i = 0; i < 10; i++)
        {
            Instantiate(sausage, new Vector3(8.0f, 2.5f, -4.7f), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
