﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabNThrow : MonoBehaviour
{
    private GameObject questUI;
    private Transform player;
    private GameObject playerCameraGO;
    private Transform playerCam;
    private Camera cam;
    bool beingCarried = false;
    private bool touched = false;
    private bool hasQuest = false;
    private Transform carriedObj;
    Vector2 mouseLook;
    Vector2 smoothV;
    private bool questIsComplete;
    private GameObject customer;
    private GameObject whatWeHit;

    float sensitivity = 5.0f;
    float smoothing = 2.0f;


    private bool questIsVisible;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Character").transform;
        cam = GameObject.Find("CharacterCamera").GetComponent<Camera>();
        playerCam = cam.transform;
        playerCameraGO = GameObject.Find("CharacterCamera");
        questIsComplete = false;
        customer = GameObject.Find("Customer");


    }

    // Update is called once per frame
    void Update()
    {


        if (beingCarried == false && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, 6.5f))
            {
                //only happens if we hit object.
                if(hit.transform.name == "Customer"){
                
                    //currently there exists only one customer at a time.

                    if(questIsVisible == false && hasQuest == false){
                        playerCameraGO.SendMessage("DisableMovingAround", true);
                        customer.SendMessage("DisplayQuest", true);
                        questIsVisible = true;
                        hasQuest = true;
                    }

                }
                else {
                    if (hit.rigidbody != null)
                    {
                        whatWeHit = GameObject.Find(hit.transform.name);
                        hit.rigidbody.isKinematic = true;
                        hit.transform.parent = playerCam;
                        beingCarried = true;
                        carriedObj = hit.transform;


                    }
                }
            }
        }else if (beingCarried)
        {
            if (touched)
            {
                //later
            }
            if (Input.GetMouseButtonDown(0))
            {

                if (hasQuest == true && customer.GetComponent<Customer>().GetQuest().getIsCompleted())
                {
                    //check if current quest is complete and if so, hand over the hotdog and let customer go away.
                    RaycastHit hit;
                    if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, 6.5f))
                    {
                        whatWeHit.transform.parent = customer.transform.GetChild(0);
                        whatWeHit.transform.localPosition = new Vector3(0, 0, 0);
                        customer.GetComponent<Customer>().setCustomerGotHotDog();
                        player.SendMessage("QuestDone");
                        transform.parent = null;
                        beingCarried = false;

                    }

                }
                else{
                    //this is simply dropping the object.
                    GetComponent<Rigidbody>().isKinematic = false;
                    transform.parent = null;
                    beingCarried = false;
                }

            }else if (Input.GetMouseButton(1) == true)
            {

                //get the hit, store it in a variable and apply the mouse movements here. - TODO: needs improvement.

                var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

                md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
                smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
                smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
                mouseLook += smoothV;

                carriedObj.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                carriedObj.localRotation = Quaternion.AngleAxis(mouseLook.x, carriedObj.transform.up);
            }
            else if(Input.GetMouseButton(2) == true)
            {
                GetComponent<Rigidbody>().isKinematic = false;
                GetComponent<Rigidbody>().AddForce(playerCam.forward * 500.0f);
                transform.parent = null;

            }
        }



    }

    public void resetIsVisibleVar(){
        questIsVisible = false;
    }

    public void questIsDone(){
        questIsComplete = true;
    }

}

