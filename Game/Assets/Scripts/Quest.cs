﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest
{
    public int requiredHotDogs;
    private string whatCustomerWants;
    private string description;
    private int xpPoints;
    private int earnedMoney;
    private bool isCompleted;
    private List<string> possibleDescription = new List<string>();

 
    public Quest()
    {
        xpPoints = 100;
        earnedMoney = 3;
        requiredHotDogs = getFood();
        possibleDescription.Add("Hi there. I would like to have some HotDogs.");
        possibleDescription.Add("Hi, can you please give me some HotDogs?");
        possibleDescription.Add("My mum sent me to buy some HotDogs.");

        description = possibleDescription[Random.Range(0, 2)];
        if(requiredHotDogs != 1){
            whatCustomerWants = "HotDogs";
        }
        else{
            whatCustomerWants = "HotDog";
        }


    }

    private int getFood(){

        // int amount = Random.Range(1, 3);
        int amount = 1;

        earnedMoney = earnedMoney * amount;
        xpPoints = xpPoints * amount;

        return amount;
    }

    public int getRequiredHotDogs(){
        return requiredHotDogs;
    }

    public string getDescription(){
        return description;
    }

    public int getXpPoints(){
        return xpPoints;
    }

    public int getEarnedMoney(){
        return earnedMoney;
    }

    public bool getIsCompleted(){
        return isCompleted;
    }

    public string getWhatCustomerWants(){
        return whatCustomerWants;
    }

    public void setIsCompleted(){
        isCompleted = true;
    }

}
